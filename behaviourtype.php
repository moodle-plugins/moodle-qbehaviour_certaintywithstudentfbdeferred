<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Question behaviour type for deferred feedback with certainty and student feedback behaviour.
 * @package    qbehaviour_certaintywithstudentfbdeferred
 * @copyright  2021 Astor Bizard <astor.bizard@univ-grenoble-alpes.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

use core\chart_bar;
use core\chart_series;
use qbehaviour_certaintywithstudentfbdeferred\answerclass;
use qbehaviour_certaintywithstudentfbdeferred\answersubcategory;
use qbehaviour_certaintywithstudentfbdeferred\certaintylevel;
use qbehaviour_certaintywithstudentfbdeferred\locallib;

require_once(__DIR__ . '/../studentfeedbackdeferred/behaviourtype.php');

/**
 * Question behaviour type information for deferred feedback with certainty and student feedback behaviour.
 * @copyright  2021 Astor Bizard <astor.bizard@univ-grenoble-alpes.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class qbehaviour_certaintywithstudentfbdeferred_type extends qbehaviour_studentfeedbackdeferred_type {

    /**
     * {@inheritDoc}
     * @see question_behaviour_type::is_archetypal()
     */
    public function is_archetypal() {
        return true;
    }

    /**
     * {@inheritDoc}
     * @see question_behaviour_type::can_questions_finish_during_the_attempt()
     */
    public function can_questions_finish_during_the_attempt() {
        return false;
    }

    /**
     * {@inheritDoc}
     * @see question_behaviour_type::summarise_usage()
     * @param question_usage_by_activity $quba the usage to provide summary data for.
     * @param question_display_options $options options about what to display.
     */
    public function summarise_usage(question_usage_by_activity $quba, question_display_options $options) {
        global $OUTPUT, $CFG;
        $summarydata = parent::summarise_usage($quba, $options);

        if ($options->marks < question_display_options::MARK_AND_MAX) {
            return $summarydata;
        }

        require_once($CFG->libdir . '/graphlib.php');
        $nlevels = count(certaintylevel::get_levels());
        $ncategories = count(answersubcategory::get_subcategories());
        $oursummary = '';

        // Categorize all answers, and sum up wrong and right answers.
        $subcategorysizes = array_fill(0, $ncategories, 0);
        $nwrong = 0;
        $nright = 0;
        foreach ($quba->get_attempt_iterator() as $qa) {
            $subcategory = answersubcategory::subcategorize_answer($qa);
            if ($subcategory !== null) {
                $subcategorysizes[$subcategory->index] ++;
                if ($subcategory->answerclass->correctness === 'correct') {
                    $nright ++;
                } else if ($subcategory->answerclass->correctness === 'incorrect') {
                    $nwrong ++;
                }
            }
        }

        $hasdeclaredignorance = locallib::exists_level_of_declared_ignorance();

        $oursummary .= get_string('ncorrectanswers', locallib::COMPONENT, html_writer::span($nright, 'numanswers')) . '<br>';
        if ($hasdeclaredignorance) {
            $oursummary .= get_string('ndeclaredignorance', locallib::COMPONENT,
                    html_writer::span($subcategorysizes[$nlevels - 1], 'numanswers')) . '<br>';
            $objectives = [
                    [ 0, 0.3, 1, 3, 8, null, null, null, null, null, null ],
                    [ null, null, null, null, null, null, 2, 3, 5, 10, 24 ],
            ];
        } else {
            $objectives = [
                    [ 0, 0.3, 1, 2.5, 6.2, 13, null, null, null, null, null, null ],
                    [ null, null, null, null, null, null, 1.5, 2, 3, 5, 10, 24 ],
            ];
        }
        $oursummary .= get_string('nincorrectanswers', locallib::COMPONENT, html_writer::span($nwrong, 'numanswers')) . '<br>';

        // Determine objective according to number of right and wrong answers.
        $coef = ($nwrong + $nright) / (array_sum($objectives[0]) + array_sum($objectives[1])) * 0.5;
        $objectivemap = function($x) use($coef) {
            return ($x === null) ? null : $x * $coef;
        };

        // Draw chart.
        $chart = new chart_bar();
        $chart->set_labels(array_map('strip_tags', array_column(answersubcategory::get_subcategories(), 'detailedlabel')));

        $expectedtrendcolor = 'black';
        foreach ($objectives as $objective) {
            $objective = array_map($objectivemap, $objective);
            $objectiveseries = new chart_series(get_string('expectedtrend', locallib::COMPONENT), $objective);
            $objectiveseries->set_type(chart_series::TYPE_LINE);
            $objectiveseries->set_smooth(true);
            $objectiveseries->set_color($expectedtrendcolor);
            $objectiveseries->set_labels(null);
            $chart->add_series($objectiveseries);
        }

        $mainseries = new chart_series(get_string('numofanswers', locallib::COMPONENT), $subcategorysizes);
        $mainseries->set_colors(array_column(answersubcategory::get_subcategories(), 'color'));
        $chart->add_series($mainseries);

        // If supported, remove default legend and display our custom legend.
        if (method_exists($chart, 'set_legend_options')) { // This method exists since Moodle 3.9.
            $chart->set_legend_options([ 'display' => false ]);
            $templatecontext = new stdClass();
            $templatecontext->displaydeclaredignorance = $hasdeclaredignorance;
            $templatecontext->unexpectederrorcolor = answerclass::get_classes()[0]->color;
            $templatecontext->allegederrorcolor = answerclass::get_classes()[1]->color;
            $templatecontext->declaredignorancecolor = answerclass::get_classes()[2]->color;
            $templatecontext->unsureknowledgecolor = answerclass::get_classes()[3]->color;
            $templatecontext->sureknowledgecolor = answerclass::get_classes()[4]->color;
            $templatecontext->expectedtrendcolor = $expectedtrendcolor;
            $templatecontext->expectedtrendhelpicon = $OUTPUT->help_icon('expectedtrend', locallib::COMPONENT);
            $toplegend = $OUTPUT->render_from_template(locallib::COMPONENT . '/charttoplegend', $templatecontext);
        } else {
            $toplegend = '';
        }

        $renderedchart = $OUTPUT->render($chart);

        // Insert custom legend between chart and chart data table.
        $sep = strpos($renderedchart, '<div class="chart-table');

        $templatecontext = new stdClass();
        $templatecontext->answercategoriespercentages = [];
        foreach (answersubcategory::get_subcategories() as $answersubcategory) {
            $templatecontext->answercategoriespercentages[] = $answersubcategory->certaintylevel->percentage;
        }
        $templatecontext->answerclasses = [];
        $i = 0;
        foreach (answerclass::get_classes() as $answercategory) {
            $class = new stdClass();
            $class->display = $answercategory->nsubcategories > 0;
            $class->spacepercentage = $answercategory->nsubcategories * round(100.0 / $ncategories);
            $class->name = get_string($answercategory->name . 'plural', locallib::COMPONENT);
            $class->numanswers = array_sum(array_slice($subcategorysizes, $i, $answercategory->nsubcategories));
            $templatecontext->answerclasses[] = $class;
            $i += $answercategory->nsubcategories;
        }

        $legend = $OUTPUT->render_from_template(locallib::COMPONENT . '/chartbottomlegend', $templatecontext);

        $oursummary .= $toplegend . substr($renderedchart, 0, $sep) . $legend . substr($renderedchart, $sep);

        $summarydata[locallib::COMPONENT . '_summary'] = [
                'title' => get_string('behavioursummary', locallib::COMPONENT),
                'content' => html_writer::div($oursummary, 'qbehaviour-certaintywithstudentfbdeferred'),
        ];

        // Put student feedback field after the chart.
        if (isset($summarydata['qbehaviour_studentfeedbackdeferred_summary'])) {
            $feedbackfield = $summarydata['qbehaviour_studentfeedbackdeferred_summary'];
            unset($summarydata['qbehaviour_studentfeedbackdeferred_summary']);
            $summarydata['qbehaviour_studentfeedbackdeferred_summary'] = $feedbackfield;
        }

        // From Moodle 4.1 onwards, ChartJS does display labels along the X axis.
        // We do not want these labels as they are quite long for our chart.
        // Unfortunately, the moodle chart API does not provide a way to edit these options,
        // so we have to edit them via JS in postprocessing.
        // The following code removes labels on X axis, removes non-integers on the Y axis, and sets the Y axis title.
        global $PAGE;
        $PAGE->requires->js_amd_inline("require(['core/chartjs'], function(ChartJS) {
            var removeXLabels = function() {
                var chart = ChartJS.getChart(document.querySelector('.quizreviewsummary .chart-area canvas'));
                if (typeof chart === 'undefined') {
                    setTimeout(removeXLabels, 50);
                } else {
                    chart.options.scales.x.ticks.display = false;
                    chart.options.scales.y.ticks.stepSize = 1;
                    chart.options.scales.y.title.display = true;
                    chart.options.scales.y.title.text = '" .
                        addslashes(get_string('numofanswers', locallib::COMPONENT)) .
                    "';
                    chart.update();
                }
            };
            removeXLabels();
        });");

        return $summarydata;
    }

}
