<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qbehaviour_certaintywithstudentfbdeferred', language 'en'
 * @package    qbehaviour_certaintywithstudentfbdeferred
 * @copyright  2021 Astor Bizard <astor.bizard@univ-grenoble-alpes.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['allegederror'] = 'Presumed error';
$string['allegederror_details'] = 'Presumed error ({$a})';
$string['allegederrorplural'] = 'Presumed errors';
$string['almostsure'] = 'Almost sure';
$string['almostsure_details'] = 'you were almost sure';
$string['answer'] = 'your answer was {$a}';
$string['answercategorydetails'] = '{$a->certainty} and {$a->youranswer}';
$string['behavioursummary'] = 'Certainty summary';
$string['correct'] = 'correct';
$string['correctanswers'] = 'Correct answers';
$string['declaredignorance'] = 'Random answer';
$string['declaredignorance_details'] = 'Random answer (you answered at random, whether your answer was correct or incorrect does not matter)';
$string['declaredignoranceplural'] = 'Random answers';
$string['expectedtrend'] = 'Expected trend';
$string['expectedtrend_help'] = 'This describes the overall shape that the histogram would look like with a typical lucid behaviour. The closer your histogram shape is to this curve, the more lucid you were about your answers and certainty.';
$string['incorrect'] = 'incorrect';
$string['incorrectanswers'] = 'Incorrect answers';
$string['ncorrectanswers'] = 'Correct answers: {$a}';
$string['ndeclaredignorance'] = 'Random answers: {$a}';
$string['nincorrectanswers'] = 'Incorrect answers: {$a}';
$string['numofanswers'] = 'Number of answers';
$string['pleaseselectcertainty'] = 'Please select one.';
$string['pluginname'] = 'Certainty with student feedback (deferred)';
$string['pluginsettings'] = 'Certainty quiz behaviours settings';
$string['privacy:behaviourpath'] = 'Behaviour';
$string['privacy:metadata:attemptstepdata'] = 'This plugin stores some data submitted during quiz attempts.';
$string['privacy:metadata:name'] = 'Attempt step data variable name, among [certainty, studentfeedback, generalstudentfeedback].';
$string['privacy:metadata:value'] = 'Attempt step data variable value.
For certainty, certainty that submitted answer would be considered correct, for each attempted question using this behaviour.
For studentfeedback, feedback submitted after a question attempt, for each attempted question using this behaviour.
For generalstudentfeedback, global feedback submitted after a quiz attempt, for each quiz using this behaviour.';
$string['quitesure'] = 'Quite sure';
$string['quitesure_details'] = 'you were quite sure';
$string['quiteunsure'] = 'Quite unsure';
$string['quiteunsure_details'] = 'you were quite unsure';
$string['random'] = 'Fifty-fifty or less';
$string['random_alt'] = 'I answered at random';
$string['random_details'] = 'you answered at random';
$string['random_open'] = 'I believe it is wrong';
$string['settings:answercategorization'] = 'Answer categorization';
$string['settings:answerclasses'] = 'Answer classes';
$string['settings:answerclassesinfo'] = 'For each answer class, you can customize the associated color.<br>
You can customize answer class labels through <a href="{$a}">language customization settings</a>.';
$string['settings:certaintylevela'] = 'Certainty level {$a}';
$string['settings:certaintylevels'] = 'Certainty levels';
$string['settings:certaintylevelsinfo'] = 'For each certainty level, you can customize displayed label (if relevant), displayed percentage and the way the answer will be categorized upon a right / wrong answer.<br>
You can further customize certainty level labels through <a href="{$a}">language customization settings</a>.';
$string['settings:enablefbforclasses'] = 'Display student feedback field for answer classes';
$string['settings:enablefbforclasses_help'] = 'The field prompting the student for a textual feedback will only be shown for questions with an answer within selected classifications.';
$string['settings:error:categoryorder'] = 'Please keep a continuity in certainty levels categorization: first optional level of Random answers, then levels of Unsecure knowledge / Presumed errors, and finally levels of Well-founded knowledge / Unexpected errors.';
$string['settings:label'] = 'Label';
$string['settings:loadpresets'] = 'Load scale preset';
$string['settings:percentage'] = 'Percentage';
$string['settings:preset:alternative'] = 'Alternative (linear)';
$string['settings:preset:default'] = 'Default (legacy)';
$string['settings:preset:recommended'] = 'Recommended (non-linear)';
$string['settings:studentfeedback'] = 'Student feedback field';
$string['settings:useopenlabel'] = 'open questions alternative certainty label';
$string['settings:useopenlabel_help'] = 'For questions that are open (i.e. neither True/False nor Multiple choice), this alternative label can be used to describe this certainty level.';
$string['settings:useopenlabela'] = 'Use "{$a}" for open questions';
$string['settingsformerrors'] = 'Changes not saved. There were some errors in the submitted data. Please refer to related error messages below.';
$string['sure'] = 'Sure';
$string['sure_details'] = 'you were sure';
$string['sureknowledge'] = 'Well-founded knowledge';
$string['sureknowledge_details'] = 'Well-founded knowledge ({$a})';
$string['sureknowledgeplural'] = 'Well-founded knowledge';
$string['unexpectederror'] = 'Unexpected error';
$string['unexpectederror_details'] = 'Unexpected error ({$a})';
$string['unexpectederrorplural'] = 'Unexpected errors';
$string['unsure'] = 'Unsure';
$string['unsure_details'] = 'you were unsure';
$string['unsureknowledge'] = 'Unsecure knowledge';
$string['unsureknowledge_details'] = 'Unsecure knowledge ({$a})';
$string['unsureknowledgeplural'] = 'Unsecure knowledge';
$string['whatisyourcertaintylevel'] = 'What is the certainty level associated with your answer?';
