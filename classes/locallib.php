<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace qbehaviour_certaintywithstudentfbdeferred;

/**
 * Utility class for the plugin.
 * @package    qbehaviour_certaintywithstudentfbdeferred
 * @copyright  2024 Astor Bizard <astor.bizard@univ-grenoble-alpes.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class locallib {

    /**
     * @var string Frankenstyle name of this plugin
     */
    public const COMPONENT = 'qbehaviour_certaintywithstudentfbdeferred';

    /**
     * @var string Unique name for the settings page
     */
    public const SETTINGSPAGE = 'qbehaviour_certaintywithstudentfbdeferred';

    /**
     * Determine if the first certainty level is associated with declared ignorance
     * @return boolean
     */
    public static function exists_level_of_declared_ignorance() {
        return certaintylevel::get_levels()[0]->category === certaintylevel::CATEGORYRANDOM;
    }

}
